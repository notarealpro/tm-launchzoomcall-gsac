// ==UserScript==
// @name         Launch Scheduled Zoom Call
// @namespace    https://getsupport.atlassian.com
// @version      1.2.1
// @description  Open scheduled zoom call and transition ticket to 'On Call'
// @notes        ===========================================================
// @notes        ######################## IMPORTANT ########################
// @notes        ===========================================================
// @notes        Your Google Calendar Zoom event MUST include the <ISSUEKEY> in the title
// @notes        Designed to work with the default Google Cal events created whilst scheduling a call with the Zoom client
// @notes        -----------------------------------------------------------
// @author       Danny Mark
// @match        https://*.atlassian.net/browse/*
// @match        https://getsupport.atlassian.com/browse/*
// @downloadURL  https://bitbucket.org/notarealpro/tm-launchzoomcall-gsac/raw/master/zoomCallGSAC.user.js
// @updateURL    https://bitbucket.org/notarealpro/tm-launchzoomcall-gsac/raw/master/zoomCallGSAC.user.js
// @grant        GM.xmlHttpRequest
// @grant        GM_addStyle
// ==/UserScript==

(function () {
    'use strict';

    // ========== USER CONFIG ==========
    // +++++ TRANSITION TO TRIGGER +++++
    var transitionName = "Start Call"; // Transition button text (case-sensitive, for now)

    // ========= ADMIN CONFIG ==========
    // #### TRANS NAV BAR HTML DIVs ####
    var noDropdown = "#opsbar-opsbar-transitions"; // Transition navbar div
    var dropDown = "#opsbar-transitions_more_drop"; // Workflow dropdown div
    // #################################

    detectDOMchange();

    var context = window.location.pathname; // Get context path
    var ticket = context.substring(8); // Get issuekey - removes '/browse/'
    //var ticket = context.match(/[A-Z]..-\d+/); // Regex - then issuekey is ticket[0]
    var dt = new Date(); // Get days and months in case of a need to change calendar or filter button display
    var y = dt.getFullYear(); // Get year
    var m = dt.getMonth() + 1; // Get month numeral [1-12] + 1 (digit returned is one month behind, not sure why yet. Must start at 0 but date doesn't.)
    var d = dt.getDate(); // Get day numeral [1-31]
    // Google Calendar URL syntax at bottom of page
    var i;
    var idToClick;


    function launchCall(zoomLink, idToClick) {
        $('#zoomCall').unbind().click(function () {
            $(idToClick).click();
            console.log("On Call transition successful.");
            window.open(zoomLink);
            console.log("Starting Zoom call: " + ticket + ": " + zoomLink + " TransID: " + idToClick);
        });
    }

    function getZoomEvents() {
        function findIdFromString(id, transitionName) { //( findIdFromString"#opsbar-opsbar-transitions", "Mark as Done")
            if (id == dropDown) {
                var transNavDropdown = $(id);
                var x = transNavDropdown[0].childNodes[0].children[0].children;
                //console.log("Dropdown found");
            } else {
                var transNavBtns = $(id);
                var x = transNavBtns[0].children;
                //console.log("Dropdown not found");
            }
            for (i = 0; i < x.length; i++) {
                if (x[i].innerText == transitionName && x[i].id != "") { // Need to use != "" because !=/== null is always false below, so easier to keep it the same
                    var idToClick = "#" + x[i].id; // This for Cloud, or NO server workflow dropdown
                    //console.log("String from UI: " + x.item(i).innerText + "\nMatches our transition button: " + transitionName + " found on navbar\nButton ID: " + idToClick);
                } else if (x[i].innerText == transitionName && x[i].id == "") { // Need to use == "" because !=/== null is always false
                    var idToClick = "#" + x[i].childNodes[0].id;
                    //console.log("String from UI: " + x.item(i).innerText + "\nMatches our transition button: " + transitionName + " found under the navbar dropdown\nButton ID: " + idToClick);
                } else {
                    // DO STUFF IF ALL ELSE FAILS OR YOU NEED ANOTHER CONDITION etc.. I'm lazy.
                    //console.log("Where does he get all these wonderful toys?")
                }
            }

            GM.xmlHttpRequest({ // Used to bypass CORS whilst calling GCal from GSAC - user permission required
                method: "GET",
                url: "https://calendar.google.com/calendar/r/day/" + y + "/" + m + "/" + d, // Searches Google Calendar for todays events
                headers: {
                    "User-Agent": "Mozilla/5.0", // If not specified, navigator.userAgent will be used.
                    "Accept": "text/html" // If not specified, browser defaults will be used.
                },
                onload: function (response) {
                    // Need to use the RegExp() function to be able to inlcude variables in the regex. Not possible using .match()
                    let ticketRegex = new RegExp(ticket + ".*us.j\/[0-9]+", "igm"); // Creates a new regex query & allows the use of variables
                    let matchedTicketCalls = response.responseText.match(ticketRegex); // Extract from the issuekey up until the end of the zoom link
                    let results = ticketRegex.test(matchedTicketCalls); // Outputs TRUE:FALSE if regex matches string

                    if (results == true) {
                        let latestMatchedCall = matchedTicketCalls[matchedTicketCalls.length - 1]; // Grabs only the last found matchedTicketCalls from the array returned
                        var latestZoomLink = (String(latestMatchedCall).match(/https:\/\/atlassian\.zoom\.us\/j\/.*/)); // Extract only the Zoom meeting URL from latestmatchedTicketCalls  // Only display button if regex matches
                        $(idToClick).hide();
                        console.log("Scheduled Zoom call found for: " + ticket, '\n', "Zoom meeting link: " + latestZoomLink, '\n', "Time: I'm not that smart yet :(");
                        console.log(transitionName + " button found.\nID: " + idToClick + "\nHiding 'Start Call' button.");
                        createBtn("zoomCall", "Zoom Call"); // Adds button by calling createBtn() function
                        launchCall(latestZoomLink, idToClick);
                    } else {
                        console.log("No scheduled Zoom call found for today.");
                    }
                }
            });
        }

        // Checks where button exists under different scenarios
        if ($(noDropdown).length > 0 && $(dropDown).length > 0 && $(noDropdown)[0].innerText.match(transitionName) != null) {
            // Checks for navbar AND dropdown button AND transition name is found on navbar
            findIdFromString(noDropdown, transitionName);
            console.log("Dropdown 'Workflow' button is true\n" + transitionName + " found in navbar - not in dropdown");

        } else if ($(noDropdown).length > 0 && $(dropDown).length > 0 && $(dropDown)[0].innerText.match(transitionName) != null) {
            // Checks for navbar AND transition name is found in dropdown
            findIdFromString(dropDown, transitionName);
            console.log("Dropdown 'Workflow' button is true\n" + transitionName + " found in dropdown.");

        } else if ($(noDropdown).length > 0 && $(noDropdown)[0].innerText.match(transitionName) != null) {
            // Checks for navbar AND transition button on navbar
            findIdFromString(noDropdown, transitionName);
            console.log("Transition navbar found + " + transitionName + " button found.");
        } else {
            console.log(transitionName + " Not possible from here");
        }
    }

    getZoomEvents(); // Start your engines


    function createBtn(id, text) {
        let btn = document.createElement("BUTTON");
        btn.id = (id);
        btn.type = "button";
        btn.className = "gsacBtn";
        let t = document.createTextNode(text);
        btn.appendChild(t); // Append the text to <button>
        document.getElementById("opsbar-opsbar-transitions").appendChild(btn); // GSAC Toolbar
    }

    function detectDOMchange() {
        var target = document.querySelector('#stalker');
        // create an observer instance
        var observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                //console.log(mutation.type);
                console.log("Mutation type detected: " + mutation.type);
                getZoomEvents();
            });
        });

        var config = { // configuration of the observer:
            childList: true // Mutation.type to look for
        };

        observer.observe(target, config); // pass in the target node, as well as the observer options
    }

    //getZoomEvents();                                        // Run the whole shebang
//            border:                 1px solid #ccc;
    GM_addStyle(`
        .gsacBtn {
            border:                 0px;
            border-radius:          3px;
            padding:                4px 10px;
            margin:                 0 5px 0 0;
            background:             #f5f5f;
            background-color:       #187dd68a;
            color:                  #333;
            height:                 30px;
            font-size:              14px;
        }
        .gsacBtn:hover {
            background-color:       RoyalBlue !important;
        }
        .gsacBtn:first-child {
            border-top-left-radius: 3.01px;
            border-bottom-left-radius: 3.01px;

        }
        .gsacBtn:last-child {
            border-top-right-radius: 3.01px;
            border-bottom-right-radius: 3.01px;
        }
        #zoomCall.gsacBtn {
        margin: 0 0 0 10px;
            border-top-left-radius: 3.01px;
            border-bottom-left-radius: 3.01px;
        }
        #opsbar-transitions_more {
            border-top-right-radius: 3.01px;
            border-bottom-right-radius: 3.01px;
        }
    `);
})();